/**
 * MuleSoft Examples
 * Copyright 2014 MuleSoft, Inc.
 *
 * This product includes software developed at
 * MuleSoft, Inc. (http://www.mulesoft.com/).
 */

package helloworld;

import javax.jws.WebService;

@WebService(endpointInterface = "helloworld.Greeter",serviceName="Greeter")
public class GreeterService implements Greeter
{
	@Override
    public String greet(String name)
    {
        return "Hello " + name;
    }
}
